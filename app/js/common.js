$(document).ready(function(){

    $(".owl-carousel").owlCarousel({
        loop: true,
        items: 1,
        nav: true,
        dots: true,
        center: true,

        margin: 30
        // dotsData: true
        // autoWidth: true
    });

    //tab
    $('.tab-item').on('click', function (e) {
        e.preventDefault();
        let thisEl = $(this);
        let tabContentId = thisEl.attr('data-tab-id');
        $('.tab-item').removeClass('active');
        $('.tab-item-content').removeClass('active');
        thisEl.addClass('active');
        $('#tab-item-content-' + tabContentId).addClass('active');
    })

    var sliderServises = new Swiper('.slider-servises', {
        slidesPerView: 3,
        spaceBetween: 15,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 2,

            },
            410: {
                slidesPerView: 1,
                spaceBetween: 10,
            }
        }
    });


});
